var http = require('http');
var fs = require('fs');
var path = require('path');
var url = require('url');
var querystring = require('querystring');


http.createServer(function (request, response) {
    console.log('request starting...');

    var filePath = '.' + request.url;
    if (filePath == './assets')
        filePath = './index.html';

    console.log("url -> "+ request.url);
    console.log("filePath -> "+ filePath);
    var extname = path.extname(filePath);
    var contentType = 'text/html';
    switch (extname) {
        case '.js':
            contentType = 'text/javascript';
            break;
        case '.css':
            contentType = 'text/css';
            break;
        case '.json':
            contentType = 'application/json';
            break;
        case '.png':
            contentType = 'image/png';
            break;      
        case '.jpg':
            contentType = 'image/jpg';
            break;
        case '.wav':
            contentType = 'audio/wav';
            break;
    }

    if (request.url.indexOf("/update-json") != -1) {
        // var str = JSON.stringify(request, null, 4);
        console.log(request.body);
        console.log(request.data);
        // for (x in request) {
        //     console.log(request);
        // }

        // todo: retornar json atualizado
        // logica atualiza arquivo json
        var q = querystring.parse(request.url);
        console.log(q);
        var inputs = [
            q["/update-json?input1"],
            q.input2,
            q.input3,
            q.input4,
            q.input5,
            q.input6,
        ]

        console.log(inputs);



        //console.log('apors quebrar as variaveis, ciar uma lista');
        //console.log('utilizar a função fs.readFile, a função me retornar um content com o conteudo do arquivo');
        //console.log('vou pegar o conteudo do arquivo e colocar em um variavel listas');
        //console.log('atualizar a lista e escrever no arquivo utilizando  função abaixo');

        fs.readFile('./assets/data.json', "utf-8", function(error, dataJson) {
            //var lista = dataJson.data;
            var lista = JSON.parse(dataJson);
            lista.data.push(inputs);

            //console.log(lista[4]);

            fs.writeFile('./assets/data.json', JSON.stringify(lista),'utf-8' , function (error,content) {
                //todo
                if (error) {
                    if(error.code == 'ENOENT'){
                        fs.readFile('./404.html', function(error, content) {
                            response.writeHead(200, { 'Content-Type': contentType });
                            response.end(content, 'utf-8');
                        });
                    }
                    else {
                        response.writeHead(500);
                        response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
                        response.end(); 
                    }
                }else{
                    contentType = 'application/json';
                    response.writeHead(200, { 'Content-Type': contentType });
                    response.end(content, 'utf-8');
                }
            });

        });


    }
    fs.readFile(filePath, function(error, content) {
        console.log("url -> "+ request.url);
        if (error) {
            if(error.code == 'ENOENT'){
                fs.readFile('./404.html', function(error, content) {
                    response.writeHead(200, { 'Content-Type': contentType });
                    response.end(content, 'utf-8');
                });
            }
            else {
                response.writeHead(500);
                response.end('Sorry, check with the site admin for error: '+error.code+' ..\n');
                response.end(); 
            }
        }
        else {
            response.writeHead(200, { 'Content-Type': contentType });
            response.end(content, 'utf-8');
        }
    });
    

}).listen(8000);
console.log('Server running at http://127.0.0.1:8000/');